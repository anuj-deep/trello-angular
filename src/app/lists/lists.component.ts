import { Component, OnInit, Input } from '@angular/core';
import {HttpClient} from '@angular/common/http'
import {Router} from '@angular/router'
import {ActivatedRoute} from '@angular/router';
@Component({
  selector: 'app-lists',
  templateUrl: './lists.component.html',
  styleUrls: ['./lists.component.css']
})
export class ListsComponent implements OnInit {
  
  public board:any;
  public listsOfBoard:any;
  public boardName:any;
  key ="key=7fb47defb4fa8ddb85e6c49af71fc950&token=cff212676a4c7fee80afb61c925373814bb8fb00df4d9d17ddc080b41d7234e0";
  _url="";
  _boardUrl="";
  _cardUrl="";
  

  constructor(private route:ActivatedRoute,private _http:HttpClient) { }

  getAllListsOfBoard(){
    return  this._http.get<any>(this._url);              
  }
  getBoardName(){
    return  this._http.get<any>(this._boardUrl);
  }
  getCardsOfList(){
    return this._http.get<any>(this._cardUrl);
  }
  ngOnInit(): void {
    let curBoard=this.route.snapshot.paramMap.get('id')
    this.board=curBoard;
    this._url=`https://api.trello.com/1/boards/${this.board}/lists?${this.key}`; //lists of a board
    this._boardUrl=`https://api.trello.com/1/boards/${this.board}?${this.key}`; // board of a list
    this.getAllListsOfBoard().subscribe(data=>{
      this.listsOfBoard=data;
      let listId=this.listsOfBoard.map((item:any)=> item.id);
      //console.log(listId);
      
      this._cardUrl=`https://api.trello.com/1/lists/${listId[0]}/cards?${this.key}`; // cards of a list
      // this.getCardsOfList().subscribe(data=>{
      //   console.log(data);
        
      // })
      //console.log(data);
                      
    })
    this.getBoardName().subscribe(data=>{
      this.boardName=data.name;
    })
  }

}

import { Component, OnInit,Input,Output,EventEmitter } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http'
@Component({
  selector: 'app-check-list',
  templateUrl: './check-list.component.html',
  styleUrls: ['./check-list.component.css']
})
export class CheckListComponent implements OnInit {
  @Input() public curCheckList:any;
  @Output() public reloadCardModal=new EventEmitter();
  key ="key=7fb47defb4fa8ddb85e6c49af71fc950&token=cff212676a4c7fee80afb61c925373814bb8fb00df4d9d17ddc080b41d7234e0";
  _deleteCheckListUrl=''
  _addCheckItemUrl=''
  showCardItemInput=false;
  reLoadCheckItem=false;
  addCheckItemToCheckList(checkItemName:any){
    return this._http.post<any>(this._addCheckItemUrl,checkItemName,{
      headers:new HttpHeaders({
        'Content-Type': 'application/text'
      })
    });
  }
  deleteCheckListFromCard(){
    return this._http.delete<void>(this._deleteCheckListUrl);
  }
  deleteCheckList(checkList:any){
    //console.log(checkList);
    this._deleteCheckListUrl=`https://api.trello.com/1/checklists/${checkList.id}?${this.key}`
    this.deleteCheckListFromCard().subscribe(()=>{
      //console.log("checklist deleted");
      this.reloadCardModal.emit(true);
      
    })
  }
  addCheckItem(checkListId:any,checkItemName:any){
    //console.log(checkListId);
    //console.log(checkItemName.value);
    this._addCheckItemUrl=`https://api.trello.com/1/checklists/${checkListId}/checkItems?${this.key}&name=${checkItemName.value}`
    this.addCheckItemToCheckList(checkItemName.value).subscribe(()=>{
      //console.log("checkitem added");
      this.reLoadCheckItem=true;
      this.ngOnInit();
    })
    this.showCardItemInput=false;
    this.reLoadCheckItem=false;
  }
  addCheckListItemsForm(){
    this.showCardItemInput=true
  }
  constructor(private _http: HttpClient) { }

  ngOnInit(): void {
  }

}

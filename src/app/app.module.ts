import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { BoardsComponent } from './boards/boards.component';
import  {HttpClientModule} from '@angular/common/http';
import { ListsComponent } from './lists/lists.component';
import { CardsComponent } from './cards/cards.component';
import { CardModalComponent } from './card-modal/card-modal.component';
import { CheckListComponent } from './check-list/check-list.component';
import { CheckListItemsComponent } from './check-list-items/check-list-items.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    BoardsComponent,
    ListsComponent,
    CardsComponent,
    CardModalComponent,
    CheckListComponent,
    CheckListItemsComponent,
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {BoardsComponent} from './boards/boards.component'
import {ListsComponent} from './lists/lists.component'
import {AppComponent} from './app.component'
const routes: Routes = [
  // {path:'',component:AppComponent},
  //{path:'',component:BoardsComponent},
  {path:'',component:BoardsComponent},
  {path:'boards',component:BoardsComponent},
  {path:'boards/:id',component:ListsComponent},
  //{path:'lists',component:ListsComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

import { Component, OnInit, Input } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http'

@Component({
  selector: 'app-cards',
  templateUrl: './cards.component.html',
  styleUrls: ['./cards.component.css']
})
export class CardsComponent implements OnInit {
  @Input() public listId:any;

  key ="key=7fb47defb4fa8ddb85e6c49af71fc950&token=cff212676a4c7fee80afb61c925373814bb8fb00df4d9d17ddc080b41d7234e0";

  _cardUrl="";
  _addCardUrl=""
  _deleteCardUrl=""
  curCardInfo:any;
  cards:any=[]
  showForm=false;
  viewCardModal=false;
  constructor(private _http:HttpClient) { }
  
  getCardsOfList(){
    return this._http.get<any>(this._cardUrl);
  }
  addNewCardToList(cardName:any){
    return this._http.post<any>(this._addCardUrl,cardName,{
      headers:new HttpHeaders({
        'Content-Type': 'application/text'
      })
    });
  }
  deleteCardFromList(){
    return this._http.delete<void>(this._deleteCardUrl)
  }
  ngOnInit(): void {
    this._cardUrl=`https://api.trello.com/1/lists/${this.listId}/cards?${this.key}`; // cards of a list
    this.getCardsOfList().subscribe(data=>{
      //console.log(data);
      this.cards=data;
    })
  }


  addNewCardForm(event:any){
    this.showForm=true;
    //console.log(event.target);
    
  }
  addNewCard(listId:any,cardName:any){
    this._addCardUrl=`https://api.trello.com/1/cards?${this.key}&idList=${listId}&name=${cardName.value}`,
    this.showForm=false;
    //console.log(cardName.value);
    this.addNewCardToList(cardName.value).subscribe(data=>{
      //console.log("card added successfully");
      this.ngOnInit();
      
    })
    
  }
  deleteCard(card:any,event:any){
    event.stopPropagation();
    //console.log(card.id);
    this._deleteCardUrl=`https://api.trello.com/1/cards/${card.id}?${this.key}`;
    this.deleteCardFromList().subscribe(()=>{console.log("card delete successfully");
    this.ngOnInit();
    })
  }
  showCardModal(card:any){
    //console.log("modal is opened");
    this.viewCardModal=true;
    this.curCardInfo=card;
  }

}

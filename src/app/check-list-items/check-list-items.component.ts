import { Component, OnInit ,Input} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http'
@Component({
  selector: 'app-check-list-items',
  templateUrl: './check-list-items.component.html',
  styleUrls: ['./check-list-items.component.css']
})
export class CheckListItemsComponent implements OnInit {
  checkListItems:any;
  @Input() public checkList:any;
  @Input() public reLoadIt:any;
  
  _checkListItemUrl=''
  key ="key=7fb47defb4fa8ddb85e6c49af71fc950&token=cff212676a4c7fee80afb61c925373814bb8fb00df4d9d17ddc080b41d7234e0";
  constructor(private _http: HttpClient) { }
  getCheckListItems(){
    return this._http.get<any>(this._checkListItemUrl);
  }
  ngOnInit(): void {
    this._checkListItemUrl=`https://api.trello.com/1/checklists/${this.checkList.id}/checkItems?${this.key}`
    this.getCheckListItems().subscribe((data)=>{
      //console.log(data);
      this.checkListItems=data;
    })
  }
  ngOnChanges():void{
    this.ngOnInit();
    //console.log(this.reLoadIt);
    this.reLoadIt=false;
    
  }
  
}

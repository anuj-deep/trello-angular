import { Component, OnInit, Input } from '@angular/core';
import {HttpClient} from '@angular/common/http'
import {Router} from '@angular/router'
@Component({
  selector: 'app-boards',
  templateUrl: './boards.component.html',
  styleUrls: ['./boards.component.css']
})
export class BoardsComponent implements OnInit {
  
  allBoards:any=[];
  boardNames:any=[]

  key ="key=7fb47defb4fa8ddb85e6c49af71fc950&token=cff212676a4c7fee80afb61c925373814bb8fb00df4d9d17ddc080b41d7234e0";
  _url=`https://api.trello.com/1/members/me/boards?${this.key}`
  constructor(private _http:HttpClient,private router : Router) { }
  getAllBoards(){
    return  this._http.get<any>(this._url);              
  }
  getAllListsOfBoard(board:any){
    //console.log(board);
    this.router.navigate(['/boards',board.id]) //used to redirect page
    
  }
  ngOnInit(): void {
      this.getAllBoards().subscribe(data=>{
                        this.allBoards=data;
                        //console.log(data);
                        this.boardNames=data.map((item:any)=> item.name)                  
                      })
  }
  
  
}

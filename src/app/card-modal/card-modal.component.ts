import { Component, OnInit,EventEmitter,Input , Output } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http'
// import * as EventEmitter from 'events';

@Component({
  selector: 'app-card-modal',
  templateUrl: './card-modal.component.html',
  styleUrls: ['./card-modal.component.css']
})
export class CardModalComponent implements OnInit {
  @Output() public hideCardModal=new EventEmitter();
  @Input() public curCard:any;
  key ="key=7fb47defb4fa8ddb85e6c49af71fc950&token=cff212676a4c7fee80afb61c925373814bb8fb00df4d9d17ddc080b41d7234e0";
  _listNameCardUrl=''
  _checkListUrl=''
  _addCheckListUrl=''
  checkListOfCard:any;
  showCheckListInput=false;
  hideModal(){
    console.log("please hide it");
    this.hideCardModal.emit(false); //child to parent comp. interaction
  }
  getCurListName(){
    return this._http.get<any>(this._listNameCardUrl);
  }
  getCheckListOfCard(){
    return this._http.get<any>(this._checkListUrl);
  }
  addNewCheckListModal(){
    this.showCheckListInput=true;
  }
  addCheckListToCard(checkListName:any){
    return this._http.post<any>(this._addCheckListUrl,checkListName,{
      headers:new HttpHeaders({
        'Content-Type': 'application/text'
      })
    });
  }
  hideCheckListInput(){
    this.showCheckListInput=false;
  }
  addNewCheckList(newCheckList:any,curCard:any){
    this._addCheckListUrl=`https://api.trello.com/1/cards/${curCard.id}/checklists?${this.key}&name=${newCheckList.value}`;
    this.addCheckListToCard(newCheckList.value).subscribe((data)=>{
      //console.log("checkList added successfully");
      this.ngOnInit();
      
    })
    //console.log(newCheckList.value+" "+curCard.id);
    this.showCheckListInput=false;
  }
  curListName=""
  constructor(private _http: HttpClient) { }

  ngOnInit(): void {
    //console.log(this.curCard);
    
    this._listNameCardUrl=`https://api.trello.com/1/cards/${this.curCard.id}/list?${this.key}`
    this._checkListUrl=`https://api.trello.com/1/cards/${this.curCard.id}/checklists?${this.key}`
    this.getCurListName().subscribe(data=>{
      //console.log(data);
      this.curListName=data.name;
    })
    this.getCheckListOfCard().subscribe(data=>{
      //console.log(data);
      this.checkListOfCard=data;
    })
  }

}
